const db = require("./../db.js");

const listwp = () => {
    return new Promise((resolve, reject) => {
        db.query('SELECT *, (select count(*) from parametros as p where p.id_grupo = g.id) as parametros FROM grupos as g', (err, rows) => {
            if (err) reject(err)
            resolve(rows);
        });
    });
}

const insert = ({ nombre, descripcion }) => {
    return new Promise((resolve, reject) => {
        db.query('INSERT INTO grupos (nombre, descripcion) VALUES  (?,?)', [nombre, descripcion], (err,
            result) =>{
                if (err) reject(err)
                if (result) {
                    resolve(result)
                };
            });
    });
};



module.exports = {
    listwp : listwp,
    insert : insert
}