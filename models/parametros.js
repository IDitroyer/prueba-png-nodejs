const db = require("./../db.js");
const moment = require('moment');


const findByGrupo = (idGrupo) => {
    return new Promise((resolve, reject) => {
        db.query('select * from parametros where id_grupo = ?',[idGrupo], (err, rows) => {
            if (err) reject(err)
            resolve(rows);
        });
    });
}

const findByGrupoActivos = (id) => {
    return new Promise((resolve, reject) => {
        db.query('select * from parametros where id_grupo = ? and estado = 1',
        [id], (err, rows) => {
            if (err) reject(err)
            resolve(rows);
        });
    });
} 


const updateParametros = ({ id, id_grupo, codigo, valor, fecha, nombre, nombre_corto, estado }) => {
    const date  = moment(new Date(fecha)).format("YYYY-MM-DD HH:mm:ss")
    return new Promise((resolve, reject) => {
        db.query('update parametros set id_grupo = ? , codigo = ? , valor = ? , fecha = ? , nombre = ? , nombre_corto = ? , estado = ? where id = ?', 
        [ id_grupo, codigo, valor, date, nombre, nombre_corto, estado, id], 
        (err, result) => {
                if (err) reject(err)
                    if(result) {
                        resolve(result);
                    }
            });
    });
};


const eliminarParametros = (id) => {
    return new Promise((resolve, reject) => {
        db.query('DELETE FROM parametros WHERE id = ?', [id], (err, result) => {
            if (err) reject(err)
            if (result) {
                resolve(result);
            }
        })
    })
}



const saveParametros = ({ id_grupo, codigo, valor, fecha, nombre, nombre_corto, estado }) => {
    estado = 1;
    return new Promise((resolve, reject) => {
        db.query('INSERT INTO parametros (id_grupo, codigo, valor, fecha, nombre, nombre_corto, estado) VALUES  (?,?,?,?,?,?,?)', 
        [ id_grupo, codigo, valor, fecha, nombre, nombre_corto, estado], 
        (err, result) =>{
                if (err) reject(err)
                    if(result) {
                        resolve(result);
                    }
            });
    });
};




module.exports = {
    findByGrupo : findByGrupo,
    saveParametros: saveParametros,
    updateParametros : updateParametros,
    eliminarParametros: eliminarParametros,
    findByGrupoActivos: findByGrupoActivos
}