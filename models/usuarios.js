const db = require("./../db.js");

const getAll = () => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM usuarios', (err, rows) => {
            if (err) reject(err)
            resolve(rows);
        });
    });
}

const listarUsuarios = (id) => {
    console.log(id)
    return new Promise((resolve, reject) => {
        db.query('select *, (select count(*) from Pagos p where id_usuario = u.id) as pagos from usuarios as u where id != ?',
        [id], (err, rows) => {
            if (err) reject(err)
            resolve(rows)
        })
    })
} 


const updateUsuarios = ({id, celular, correo, identificacion, nombre, usuario, activo }) => {
    return new Promise((resolve, reject) => {
        db.query('update usuarios set celular = ? , correo = ? , identificacion = ? , nombre = ? , usuario = ? , activo = ? where id = ?', 
        [celular, correo, identificacion, nombre, usuario, activo , id], (err, result) => {
            if (err) reject(err)
            resolve(result)
        })
    })
}


const eliminarUsuarios = (id) => {
    return new Promise((resolve, reject) => {
        db.query('DELETE FROM usuarios WHERE id = ?', [id], (err, result) => {
            if (err) reject(err)
            if (result) {
                resolve(result);
            }
        })
    })
}


const insert = ({ celular, correo, identificacion, nombre, password, usuario }) => {
    return new Promise((resolve, reject) => {
        db.query('INSERT INTO usuarios (celular, correo, identificacion, nombre, password, usuario, activo) VALUES  (?,?,?,?,?,?,1)', [celular, correo, identificacion, nombre, password, usuario], (err,
            result) =>{
                if (err) reject(err)
                if (result) {
                    db.query("INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (?, ?)", [result.insertId, 2], (err, 
                        result) => {
                            resolve(result)
                        }); 
                };
            });
    });
};


const getByUsuario = (usuario) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM usuarios WHERE usuario = ?', [usuario], (err, rows) => {
            if (err) reject(err)
            resolve(rows[0])
        });
    })
}


const getUserRol = (id) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT r.id, r.nombre FROM usuarios_roles as u INNER JOIN roles as r ON u.roles_id = r.id WHERE u.usuario_id = ?', [id], (err,
            rol) => {
                console.log(rol[0]);
                if (err) reject(err)
                resolve(rol[0])
            });
    });
}

const getByEmail = (correo) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM usuarios WHERE email = ?', [correo], (err, rows) => {
            if (err) reject(err)
            resolve(rows[0])
        });
    });
};


const getById = (id) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM usuarios WHERE id = ?', [id], (err, rows) => {
            if (err) reject(err)
            resolve(rows[0])
        });
    });
};


module.exports = {
    getAll: getAll,
    insert: insert,
    getByEmail: getByEmail,
    getByUsuario: getByUsuario,
    getById: getById,
    getUserRol: getUserRol,
    listarUsuarios: listarUsuarios,
    updateUsuarios: updateUsuarios,
    eliminarUsuarios: eliminarUsuarios  
}