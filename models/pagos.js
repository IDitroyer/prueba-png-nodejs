const db = require("./../db.js");
const moment = require('moment');

const getByUsuario = (id) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT p.id, p.id_usuario, p.fecha, pa.nombre as nombre, pa.id as id_parametro, p.soporte, p.activo, p.referencia FROM pagos as p INNER JOIN parametros as pa ON p.tipo = pa.id where p.id_usuario = ? ',
        [id], (err, rows) => {
            if (err) reject(err)
            resolve(rows);
        });
    });
}



const saveSoporte = (id, archivo) => {
    return new Promise((resolve, reject) => {
        db.query('update pagos set soporte = ? where id = ?', 
        [ archivo, id], 
        (err, result) => {
                if (err) reject(err)
                    if(result) {
                        resolve(result);
                    }
            });
    });
}


const getByIdUsuario = (id) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT p.id, p.id_usuario, p.fecha, pa.nombre as nombre, pa.id as id_parametro, p.soporte, p.activo, p.referencia FROM pagos as p INNER JOIN parametros as pa ON p.tipo = pa.id where p.id_usuario = ?',
        [id], (err, rows) => {
            if (err) reject(err)
            resolve(rows);
        });
    });
}


const eliminarPago = (id) => {
    return new Promise((resolve, reject) => {
        db.query('DELETE FROM pagos WHERE id = ?', [id], (err, result) => {
            if (err) reject(err)
            if (result) {
                resolve(result);
            }
        })
    })
}






const savePago = ({ id_usuario, fecha, tipo, referencia }) => {
    const date  = moment(new Date(fecha)).format("YYYY-MM-DD HH:mm:ss")
    return new Promise((resolve, reject) => {
        db.query('INSERT INTO pagos (id_usuario, fecha, tipo, activo, referencia) VALUES  (?,?,?,?,?)', 
        [ id_usuario, date, tipo,1 ,  referencia ], 
        (err, result) =>{
                if (err) reject(err)
                    if(result) {
                        resolve(result);
                    }
            });
    });
};



module.exports = {
    getByUsuario: getByUsuario,
    savePago: savePago,
    eliminarPago: eliminarPago,
    saveSoporte: saveSoporte,
    getByIdUsuario: getByIdUsuario
}