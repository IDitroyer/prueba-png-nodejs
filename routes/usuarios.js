var express = require('express');
const usuarios = require('../models/usuarios');
var router = express.Router();
const Users = require('../models/usuarios');
const bcrypt = require('bcrypt');
const jwt = require('jwt-simple');
const moment = require('moment');
const middleware = require('./middleware');



router.post('/register', async (req, res) => {
  console.log(req.body);
  req.body.password = bcrypt.hashSync(req.body.password, 10);
  const result = await usuarios.insert(req.body);
  res.json(result);
});

const createToken = (usuario, rol) => {
  let payload = {
    id: usuario.id,
    rol : rol,
    createdAt: moment().unix(),
    expiresAt: moment().add(1, 'day').unix()
  }
  return jwt.encode(payload, process.env.TOKEN_KEY);
}




router.post('/login', async (req, res) => {
  const user = await Users.getByUsuario(req.body.usuario)
  if (user === undefined) {
    res.json({
      error : 'Error, credenciales incorrectas'
    })
  } else {
    const equals = bcrypt.compareSync(req.body.password, user.password);
    if (!equals ) {
      res.json({
        error : 'Error, credenciales incorrectas'
      });
    } else {
      const rol = await usuarios.getUserRol(user.id)
      res.json({
        accessToken : createToken(user, rol)
      });
    }
  }
})


router.use(middleware.checkToken);



router.post('/custom/save', async (req, res) => {
  const result = await usuarios.updateUsuarios(req.body);
  res.json({
    message : 'Se ha actualizado el usuario',
    result : result,
});
})



router.delete('/delete/:id', (req, res) => {
  var id = req.params.id
  usuarios.eliminarUsuarios(id)
      .then(result => {
          res.json({
            message : 'El usuario se ha eliminado correctamente',
            result : result
          });
      });
});

router.get('/list', async (req, res) => {
  usuarios.listarUsuarios(req.id)
      .then(rows => {
        res.json(rows);
      })
      .catch(err => console.log(err));
})


router.get('/me', (req, res ) => {
  Users.getById(req.id)
    .then(rows => {
      res.json(rows);
    })
    .catch(err => console.log(err));
});



module.exports = router;
