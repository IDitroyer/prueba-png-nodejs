var express = require('express');
var router = express.Router();
const Parametros = require('../models/parametros');
const middleware = require('./middleware');


router.use(middleware.checkToken);

  

    router.post('/save', async (req, res) => {

        if (req.body.id != null) {
            const result = await Parametros.updateParametros(req.body);
        } else {
            const result = await Parametros.saveParametros(req.body);
        }
        
        res.json({
            message : 'Se ha actualizado el parametro'
        });
    })


    router.delete('/delete/:id', (req, res) => {
        var id = req.params.id
        Parametros.eliminarParametros(id)
            .then(result => {
                res.json(result);
            });
    });

    router.get('/grupo/activos/:id', (req, res ) => {
        var id = req.params.id
        Parametros.findByGrupoActivos(id)
          .then(rows => {
            res.json(rows);
          })
          .catch(err => console.log(err));
      });

  router.get('/grupo/:idGrupo', (req, res ) => {
    var idGrupo = req.params.idGrupo
    Parametros.findByGrupo(idGrupo)
      .then(rows => {
        res.json(rows);
      })
      .catch(err => console.log(err));
  });
  

  module.exports = router;