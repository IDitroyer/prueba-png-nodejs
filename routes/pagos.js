const { Router } = require('express');
var express = require('express');
var router = express.Router();
const Pagos = require('../models/pagos');
const middleware = require('./middleware');
const path = require("path");

router.get("/ver/:image", (req, res) => {
    console.log(__dirname)
    res.sendFile(path.join(`${process.env.FILES_FOLDER}${req.params.image}`));
  });


router.use(middleware.checkToken);


router.post('/crear', async (req, res) => {
    const result = await Pagos.savePago(req.body);
    res.json({
        message : 'Se ha guardado el pago',
        result : result 
    });

})

router.delete('/eliminar/:id', (req, res) => {
    var id = req.params.id
    Pagos.eliminarPago(id)
        .then(result => {
            res.json(result);
        });
});





router.post('/subirsoporte',(req,res) => {
    let idPago = req.body.idPago;
    console.log(idPago)
    let EDFile = req.files.soporte
    let nombreArchivo = `${new Date().getTime()}_${EDFile.name}`;
    EDFile.mv(`${process.env.FILES_FOLDER}${nombreArchivo}`,err => {
        if(err) return res.status(500).send({ message : err })

        Pagos.saveSoporte(idPago, nombreArchivo)
            .then(result => {
                res.json({
                    message : 'El soporte se ha subido satisfactoriamente',
                    result : result
                });
            })
            .catch(err => console.log(err));
    })
})


router.get('/usuario', (req, res ) => {
    var id = req.id
    Pagos.getByIdUsuario(id)
      .then(rows => {
        res.json(rows);
      })
      .catch(err => console.log(err));
  });


  router.get('/usuario/:id', (req, res ) => {
    var id = req.params.id
    Pagos.getByUsuario(id)
      .then(rows => {
        res.json(rows);
      })
      .catch(err => console.log(err));
  });
  

  module.exports = router;