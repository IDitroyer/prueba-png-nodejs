var express = require('express');
var router = express.Router();
const Grupos = require('../models/grupos');
const bcrypt = require('bcrypt');
const jwt = require('jwt-simple');
const moment = require('moment');
const middleware = require('./middleware');


router.use(middleware.checkToken);

  
  router.get('/listwp', (req, res ) => {
    Grupos.listwp()
      .then(rows => {
        res.json(rows);
      })
      .catch(err => console.log(err));
  });
  

  module.exports = router;